CXX 	= emcc
OUTPUT 	= build/imgui.js
PAGES_OUTPUT = public/imgui.js
CFLAGS = -O2
# COPYFILES = imgui.html test.ps1
IMGUI_DIR:=./NodeEditorWasm/imgui
IMNODES_DIR:=./NodeEditorWasm/ImNodes
SRC_Dir:=./NodeEditorWasm/wasm
# EMSDK_DIR:=C:/Users/ADMIN/emsdk/upstream/emscripten/cache/sysroot/include
# EMSDK_DIR:=C:/Users/thuy.nguyen/emsdk/upstream/emscripten/cache/sysroot/include
# VCPKG_DIR:=C:/Users/ADMIN/scoop/apps/vcpkg/current/installed/x64-windows/include
# PYINCLUDE=C:/Users/ADMIN/scoop/apps/miniconda3/current/envs/c++/include
# UCRT="C:/Program Files (x86)/Windows Kits/10/Include/10.0.20348.0/ucrt"
# MSVCINCLUDE="C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.29.30133/include"

SOURCES = $(SRC_Dir)/main.cpp
SOURCES += $(IMGUI_DIR)/backends/imgui_impl_glfw.cpp $(IMGUI_DIR)/backends/imgui_impl_opengl3.cpp
SOURCES += $(IMGUI_DIR)/imgui.cpp $(IMGUI_DIR)/imgui_draw.cpp $(IMGUI_DIR)/imgui_demo.cpp $(IMGUI_DIR)/imgui_widgets.cpp $(IMGUI_DIR)/imgui_tables.cpp
SOURCES += $(filter-out $(IMNODES_DIR)/sample.cpp, $(wildcard $(IMNODES_DIR)/*.cpp) )
SOURCES += $(wildcard $(SRC_Dir)/simpleNodes/*.cpp)
SOURCES += $(wildcard $(SRC_Dir)/simpleNodes/imguihelper/*.cpp)
SOURCES += $(wildcard $(SRC_Dir)/utils/*.cpp)
SOURCES += $(wildcard $(SRC_Dir)/test/*.cpp)

# HEADER:=$(wildcard C:/Program Files (x86)/Windows Kits/10/Include/10.0.20348.0/ucrt/*.h)

LIBS = -lGL
WEBGL_VER = -s USE_WEBGL2=1 -s USE_GLFW=3 -s FULL_ES3=1
#WEBGL_VER = USE_GLFW=2
USE_WASM = -s WASM=1
USE_ALOCATE = -sEXPORTED_RUNTIME_METHODS=allocate # fix alocate missing https://github.com/emscripten-core/emscripten/issues/11547

# all: clean build/anhungxadieu copy

# _dummy := $(shell mkdir build)
# build/anhungxadieu: $(SOURCES) #$(HEADER)
# # $(CXX)  $(SOURCES) -std=c++11 -o $(OUTPUT) $(LIBS) $(WEBGL_VER) -O2 --preload-file data $(USE_WASM) -I$(IMGUI_DIR) -I$(UCRT) -I$(MSVCINCLUDE) -I$(PYINCLUDE) -I$(IMGUI_DIR)/backends
# 	# $(CXX)  $(SOURCES) -std=c++11 -lembind -sASYNCIFY -o $(OUTPUT) $(LIBS) $(WEBGL_VER) -O2 --preload-file data $(USE_WASM) $(USE_ALOCATE) -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends -I$(EMSDK_DIR) -I$(VCPKG_DIR)
# 	$(CXX)  $(SOURCES) -std=c++11 -lembind -sASYNCIFY -o $(OUTPUT) $(LIBS) $(WEBGL_VER) -O2 --preload-file data $(USE_WASM) $(USE_ALOCATE) -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends

# copy:
# 	copy $(SRC_Dir)/imgui.html build
# 	copy $(SRC_Dir)/ThuyCode.js build

# clean:
# 	del /F /Q build

pages:
	#@mkdir -p public/hello/file
	#@mkdir -p public/hello/sdl
	@mkdir -p public
	#cp index.html public/
	cp $(SRC_Dir)/imgui.html public
	cp $(SRC_Dir)/ThuyCode.js public
	#@${CC} ${CFLAGS} tests/hello_world.c -o public/hello/index.html
	#@${CC} ${CFLAGS} tests/hello_world_sdl.cpp -o public/hello/sdl/index.html
	#@${CC} ${CFLAGS} tests/hello_world_file.cpp -o public/hello/file/index.html --preload-file tests/hello_world_file.txt
	#$(CXX) $(SOURCES) -std=c++11 -lembind -sASYNCIFY -o $(PAGES_OUTPUT) $(LIBS) $(WEBGL_VER) -O2 --preload-file data $(USE_WASM) $(USE_ALOCATE) -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends -sEXPORTED_RUNTIME_METHODS=ccall,cwrap,allocate
	cp $(SRC_Dir)/build/* public

