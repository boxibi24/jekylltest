#include <emscripten.h>
#include <emscripten/val.h>

using namespace emscripten;

EM_JS(void, saveMsg, (const char * msg), {
    // alert('hello world!');
    // throw 'all done';
    // const execFile = require('child_process').execFile;
    // const child = execFile('test.ps1', [], (error, stdout, stderr) => {
    // if (error) {
    //     throw error;
    // }
    // console.log(stdout);
    // });

    // window.open(URL.createObjectURL(
    //     new Blob([JSON.stringify({"thuy":"thuythuy"})], {
    //     type: 'application/binary'}
    //     )
    // ))

    const text = UTF8ToString(msg);
    console.log(text);
    window.open(URL.createObjectURL(
        new Blob([text], {
        type: 'application/binary'}
        )
    ))
});

EM_JS(void, js_log, (const char * msg), {
    const text = UTF8ToString(msg);
    console.log(text);
});

EM_JS(EM_VAL, find_frag, (), {
    var output  = document.getElementById("output").textContent;
    return Emval.toHandle(output);
});

EM_JS(void, openFile, (), {
    js_openFile();
});

EM_JS(const char *, getCode, (const char * msg), {
    const text = UTF8ToString(msg);
    let res = js_getCode(text);
    // let res = "thuythuy";
    return allocate(intArrayFromString(res), 'i8', ALLOC_NORMAL);
});

// EM_JS(const char *, openFile, (), {
//     js_openFile();
//     let res = js_openFile();
//     // val example_json = val::take_ownership(fetch_json("https://httpbin.org/json"));
//     // Returns heap-allocated string.
//     // C/C++ code is responsible for calling `free` once unused.
//     return allocate(intArrayFromString(res), 'i8', ALLOC_NORMAL);
// });

EM_ASYNC_JS(EM_VAL, fetch_json_from_url, (const char *url_ptr), {
  var url = UTF8ToString(url);
  var response = await fetch(url);
  var json = await response.json();
  return Emval.toHandle(json);
});
