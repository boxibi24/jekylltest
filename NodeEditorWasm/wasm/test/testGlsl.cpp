// #ifndef IMGUI_DEFINE_MATH_OPERATORS
// #   define IMGUI_DEFINE_MATH_OPERATORS
// #endif
// // #include <vector>
// // #include <map>
// #include <string>
// #include <imgui.h>
// #include <imgui_internal.h>
// // #include <imgui_impl_glfw.h>
// // #include <imgui_impl_opengl3.h>

// #include <fstream>
// #include <sstream>
// #include <iostream>

// using namespace std;

// #define GLFW_INCLUDE_ES3
// #include <GLES3/gl3.h>
// #include <GLFW/glfw3.h>

// #include "thuyShader.glsl"
// #include "../utils/anhungxadieu.h"
// #include "../utils/cmds.h"

// // ---------------
// // const char *vertex_shader_code = "#version 300 es\n"
// //                                  "in vec3 aPos;\n"
// //                                  "void main()\n"
// //                                  "{\n"
// //                                  "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
// //                                  "}\0";
// // const char *fragment_shader_code =  "#version 300 es\n"
// //                                     "precision mediump float;\n"
// //                                     "out vec4 FragColor;\n"
// //                                     "void main()\n"
// //                                     "{\n"
// //                                     "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
// //                                     "}\n\0";
// // ---------------
// // #define GLSL(version, shader)  "#version " #version "\n" #shader

// // const char* vertex_shader_code = GLSL(300 es,
// // in vec3 pos;
// // void main()
// // {
// // 	gl_Position = vec4(0.9*pos.x, 0.9*pos.y, 0.5*pos.z, 1.0);
// // }
// // );

// // const char* fragment_shader_code = GLSL(300 es,
// // precision mediump float;
// // out vec4 color;
// // void main()
// // {
// // 	color = vec4(0.0, 1.0, 0.0, 1.0);
// // }
// // );

// // ---------------
// // const char* vertex_shader_code = R"END(#version 300 es
// // in vec3 pos;
// // void main()
// // {
// // 	gl_Position = vec4(0.9*pos.x, 0.9*pos.y, 0.5*pos.z, 1.0);
// // }
// // )END";

// // const char* fragment_shader_code = R"END(#version 300 es
// // precision mediump float;
// // out vec4 color;
// // void main()
// // {
// // 	color = vec4(0.0, 1.0, 0.0, 1.0);
// // }
// // )END";

// float backgroundR = 0.1f,
//       backgroundG = 0.3f,
//       backgroundB = 0.2f;

// const GLint WIDTH  = 512;
// const GLint HEIGHT = 512;

// namespace example
// {
//     namespace
//     {
//         class testGlsl
//         {
//             GLuint VAO;
//             GLuint VBO;
//             GLuint FBO;
//             GLuint RBO;
//             GLuint texture_id;
//             GLuint shader;

//             double previousTime;
// 	        int frameCount;

//             string title = "Shadertoy GLSL - 0.0.1 - ";

//             const char* vertex_shader_code = R"END(#version 300 es
//             in vec3 pos;
//             void main()
//             {
//             	gl_Position = vec4(0.9*pos.x, 0.9*pos.y, 0.5*pos.z, 1.0);
//             }
//             )END";

//             const char* fragment_shader_code = R"END(#version 300 es
//             precision mediump float;
//             out vec4 color;
//             void main()
//             {
//             	color = vec4(0.0, 1.0, 0.0, 1.0);
//             }
//             )END";

//             std::string shaderCode;

//         public:

//             GLuint loadShaderFromFile(string path, GLenum shaderType)
//             {
//                 GLuint shaderID = 0;
//                 string shaderString;
//                 ifstream sourceFile(path.c_str());
//                 if (sourceFile)
//                 {
//                     shaderString.assign((istreambuf_iterator< char >(sourceFile)), istreambuf_iterator< char >());
//                     shaderID = glCreateShader(shaderType);
//                     const GLchar* shaderSource = shaderString.c_str();
//                     glShaderSource(shaderID, 1, (const GLchar**)&shaderSource, NULL);
//                     glCompileShader(shaderID);
//                     GLint shaderCompiled = GL_FALSE;
//                     glGetShaderiv(shaderID, GL_COMPILE_STATUS, &shaderCompiled);
//                     if (shaderCompiled != GL_TRUE)
//                     {
//                         printf("Unable to compile shader %d!\n\nSource:\n%s\n", shaderID, shaderSource);
//                         glDeleteShader(shaderID);
//                         shaderID = 0;
//                     }
//                 }
//                 else
//                 {
//                     printf("Unable to open file %s\n", path.c_str());
//                 }
//                 return shaderID;
//             }

//             void create_triangle()
//             {
//                 // float vertices[] = {
//                 //     -1.0f, -1.0f, 0.0f, // 1. vertex x, y, z
//                 //     1.0f, -1.0f, 0.0f, // 2. vertex ...
//                 //     0.0f, 1.0f, 0.0f // etc... 
//                 // };

//                 // glGenVertexArrays(1, &VAO);
//                 // glBindVertexArray(VAO);

//                 // glGenBuffers(1, &VBO);
//                 // glBindBuffer(GL_ARRAY_BUFFER, VBO);
//                 // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

//                 // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
//                 // glEnableVertexAttribArray(0);

//                 // glBindBuffer(GL_ARRAY_BUFFER, 0);
//                 // glBindVertexArray(0);
                
//                 float vertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
//                     // positions   // texCoords
//                     -1.0f,  1.0f,  0.0f, 1.0f,
//                     -1.0f, -1.0f,  0.0f, 0.0f,
//                     1.0f, -1.0f,  1.0f, 0.0f,

//                     -1.0f,  1.0f,  0.0f, 1.0f,
//                     1.0f, -1.0f,  1.0f, 0.0f,
//                     1.0f,  1.0f,  1.0f, 1.0f
//                 };

//                 glGenVertexArrays(1, &VAO);
//                 glGenBuffers(1, &VBO);
//                 glBindVertexArray(VAO);
//                 glBindBuffer(GL_ARRAY_BUFFER, VBO);
//                 glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);
//                 glEnableVertexAttribArray(0);
//                 glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
//                 glEnableVertexAttribArray(1);
//                 glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
//                 glBindBuffer(GL_ARRAY_BUFFER, 0);
//                 glBindVertexArray(0);
//             }

//             void add_shader(GLuint program, const char* shader_code, GLenum type) 
//             {
//                 GLuint current_shader = glCreateShader(type);

//                 const GLchar* code[1];
//                 code[0] = shader_code;

//                 GLint code_length[1];
//                 code_length[0] = strlen(shader_code);

//                 glShaderSource(current_shader, 1, code, code_length);
//                 glCompileShader(current_shader);

//                 GLint result = 0;
//                 GLchar log[1024] = {0};

//                 glGetShaderiv(current_shader, GL_COMPILE_STATUS, &result);
//                 if (!result) {
//                     glGetShaderInfoLog(current_shader, sizeof(log), NULL, log);
//                     std::cout << "Error compiling " << type << " shader: " << log << "\n";
//                     return;
//                 }
//                 glAttachShader(program, current_shader);
//             }

//             void add_shader(GLuint program, string path, GLenum type) 
//             {

//                 // load shader from file
//                 GLuint current_shader = loadShaderFromFile(path, type);

//                 glAttachShader(program, current_shader);
//             }

//             void create_shaders()
//             {
//                 shader = glCreateProgram();
//                 if(!shader) {
//                     std::cout << "Error creating shader program!\n"; 
//                     exit(1);
//                 }

//                 add_shader(shader, vertex_shader_code, GL_VERTEX_SHADER);
//                 add_shader(shader, fragment_shader_code, GL_FRAGMENT_SHADER);
//                 // add_shader(shader, "../data/assets/v.glsl", GL_VERTEX_SHADER); // don't work :(
//                 // add_shader(shader, "../data/assets/f.glsl", GL_FRAGMENT_SHADER); // don't work :(
//                 // add_shader(shader, thuyShader::vertex, GL_VERTEX_SHADER);
//                 // add_shader(shader, thuyShader::fragment, GL_FRAGMENT_SHADER);

//                 GLint result = 0;
//                 GLchar log[1024] = {0};

//                 glLinkProgram(shader);
//                 glGetProgramiv(shader, GL_LINK_STATUS, &result);
//                 if (!result) {
//                     glGetProgramInfoLog(shader, sizeof(log), NULL, log);
//                     std::cout << "Error linking program:\n" << log << '\n';
//                     return;
//                 }

//                 glValidateProgram(shader);
//                 glGetProgramiv(shader, GL_VALIDATE_STATUS, &result);
//                 if (!result) {
//                     glGetProgramInfoLog(shader, sizeof(log), NULL, log);
//                     std::cout << "Error validating program:\n" << log << '\n';
//                     return;
//                 }
//             }

//             void create_framebuffer()
//             {
//                 glGenFramebuffers(1, &FBO);
//                 glBindFramebuffer(GL_FRAMEBUFFER, FBO);

//                 glGenTextures(1, &texture_id);
//                 glBindTexture(GL_TEXTURE_2D, texture_id);
//                 glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WIDTH, HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
//                 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//                 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//                 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_id, 0);

//                 glGenRenderbuffers(1, &RBO);
//                 glBindRenderbuffer(GL_RENDERBUFFER, RBO);
//                 glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, WIDTH, HEIGHT);
//                 glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);

//                 glClearColor(backgroundR, backgroundG, backgroundB, 1.0f);
//                 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

//                 if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
//                     std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n";

//                 glBindFramebuffer(GL_FRAMEBUFFER, 0);
//                 glBindTexture(GL_TEXTURE_2D, 0);
//                 glBindRenderbuffer(GL_RENDERBUFFER, 0);
//             }

//             void bind_framebuffer()
//             {
//                 glBindFramebuffer(GL_FRAMEBUFFER, FBO);
//             }

//             void unbind_framebuffer()
//             {
//                 glBindFramebuffer(GL_FRAMEBUFFER, 0);
//             }

//             void rescale_framebuffer(float width, float height)
//             {
//                 glBindTexture(GL_TEXTURE_2D, texture_id);
//                 glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
//                 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//                 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//                 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_id, 0);

//                 glBindRenderbuffer(GL_RENDERBUFFER, RBO);
//                 glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
//                 glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);
//             }

//             void composeDearImGuiFrame()
//             {
//                 ImGui::Begin("shader toy mini");

//                 const float window_width = ImGui::GetContentRegionAvail().x;
// 		        const float window_height = ImGui::GetContentRegionAvail().y;

//                 rescale_framebuffer(window_width, window_height);
// 		        glViewport(0, 0, window_width, window_height);

//                 ImVec2 pos = ImGui::GetCursorScreenPos();
//                 ImDrawList *drawList = ImGui::GetWindowDrawList();
//                 // drawList->AddImage((void *)texture_id,
//                 //                     pos,
//                 //                     ImVec2(pos.x + 512, pos.y + 512),
//                 //                     ImVec2(0, 1),
//                 //                     ImVec2(1, 0));
//                 drawList->AddImage((void *)texture_id,
//                                     ImVec2(ImGui::GetCursorScreenPos()),
//                                     ImVec2(ImGui::GetCursorScreenPos().x + ImGui::GetWindowSize().x - 15,
//                                         ImGui::GetCursorScreenPos().y + ImGui::GetWindowSize().y - 35),
//                                     ImVec2(0, 1),
//                                     ImVec2(1, 0));
//                 // drawList->AddImage((void *)texture_id, 
//                 //                     ImVec2(pos.x, pos.y), 
//                 //                     ImVec2(pos.x + window_width, pos.y + window_height), 
//                 //                     ImVec2(0, 1), 
//                 //                     ImVec2(1, 0)
//                 //                 );

//                 // std::string temp(fragment_shader_code);   
//                 shaderCode  = val::take_ownership(find_frag()).as<string>();                           
//                 // anhungxadieu::InputText( "fragment shader", &shaderCode );
//                 anhungxadieu::InputTextMultiline( "fragment shader", &shaderCode );

//                 fragment_shader_code = shaderCode.c_str();

//                 if (ImGui::Button("Open Shader", ImVec2(0, 0))) {
//                     openFile();
//                 }
//                 if (ImGui::Button("Compile Shader", ImVec2(0, 0))) {
//                     clean();
//                     create_shaders();
//                     create_framebuffer();
//                 }
//                 ImGui::End();
//             }

//             void clean()
//             {
//                 glDeleteFramebuffers(1, &FBO);
//                 glDeleteTextures(1, &texture_id);
//                 glDeleteRenderbuffers(1, &RBO);
//             }

//             void init()
//             {
//                 create_triangle();
//                 create_shaders();
//                 create_framebuffer();

//                 // to get time in shader
//                 previousTime = glfwGetTime();
// 	            frameCount   = 0;
//             }

//             void show()
//             {
//                 double currentTime = glfwGetTime();
//                 frameCount++;
//                 if (currentTime - previousTime >= 1.0)
//                 {
//                     string titleTemp = title + to_string(frameCount) + " fps";
//                     char* ctitle = &titleTemp[0];
//                     glfwSetWindowTitle(glfwGetCurrentContext(), ctitle);
//                     frameCount = 0;
//                     previousTime = currentTime;
//                 }

//                 composeDearImGuiFrame();

//                 bind_framebuffer();
		
//                 // glUseProgram(shader);
//                 // glBindVertexArray(VAO);
//                 // glDrawArrays(GL_TRIANGLES, 0, 3);
//                 // glBindVertexArray(0);
//                 // glUseProgram(0);

//                 glUseProgram(shader);

//                 GLint resUniform = glGetUniformLocation(shader, "iResolution");
//                 glUniform3f(resUniform, WIDTH, HEIGHT, WIDTH / HEIGHT);
//                 double mxpos, mypos;
//                 glfwGetCursorPos(glfwGetCurrentContext(), &mxpos, &mypos);
//                 GLint mouseUniform = glGetUniformLocation(shader, "iMouse");
//                 glUniform4f(mouseUniform, mxpos, mypos, mxpos, mypos);
//                 GLint timeUniform = glGetUniformLocation(shader, "iTime");
//                 glUniform1f(timeUniform, currentTime);
                
//                 glBindVertexArray(VAO);
//                 // glBindTexture(GL_TEXTURE_2D, texture_id);	// use the color attachment texture as the texture of the quad plane
//                 glDrawArrays(GL_TRIANGLES, 0, 6);
//                 glBindVertexArray(0);
//                 glUseProgram(0);
                
//                 unbind_framebuffer();	
//             }
//         };

//     static testGlsl editor;
//     } // namespace

//     void EXP_EditorInitialize() { editor.init(); }

//     void EXP_EditorShow() { editor.show(); }

//     void EXP_EditorShutdown() {editor.clean();}

// } // namespace example
