#ifndef IMGUI_DEFINE_MATH_OPERATORS
#   define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include <vector>
#include <map>
#include <string>
#include <imgui.h>
#include <imgui_internal.h>
#include "../ImNodes/ImNodesEz.h"
#include "../utils/anhungxadieu.h"
#include "../utils/cmds.h"

/// A structure defining a connection between two slots of two nodes.
struct Connection
{
    /// `id` that was passed to BeginNode() of input node.
    void* InputNode = nullptr;
    /// Descriptor of input slot.
    const char* InputSlot = nullptr;
    /// `id` that was passed to BeginNode() of output node.
    void* OutputNode = nullptr;
    /// Descriptor of output slot.
    const char* OutputSlot = nullptr;

    bool operator==(const Connection& other) const
    {
        return InputNode == other.InputNode &&
               InputSlot == other.InputSlot &&
               OutputNode == other.OutputNode &&
               OutputSlot == other.OutputSlot;
    }

    bool operator!=(const Connection& other) const
    {
        return !operator ==(other);
    }
};

enum NodeSlotTypes
{
    NodeSlotPosition = 1,   // ID can not be 0
    NodeSlotRotation,
    NodeSlotMatrix,
};

/// A structure holding node state.
struct MyNode
{
    /// Title which will be displayed at the center-top of the node.
    const char* Title = nullptr;
    /// Flag indicating that node is selected by the user.
    bool Selected = false;
    /// Node position on the canvas.
    ImVec2 Pos{};
    /// List of node connections.
    std::vector<Connection> Connections{};
    /// A list of input slots current node has.
    std::vector<ImNodes::Ez::SlotInfo> InputSlots{};
    /// A list of output slots current node has.
    std::vector<ImNodes::Ez::SlotInfo> OutputSlots{};

    std::string Code;

    explicit MyNode(const char* title,
        const std::vector<ImNodes::Ez::SlotInfo>&& input_slots,
        const std::vector<ImNodes::Ez::SlotInfo>&& output_slots)
    {
        Title = title;
        InputSlots = input_slots;
        OutputSlots = output_slots;
    }

    /// Deletes connection from this node.
    void DeleteConnection(const Connection& connection)
    {
        for (auto it = Connections.begin(); it != Connections.end(); ++it)
        {
            if (connection == *it)
            {
                Connections.erase(it);
                break;
            }
        }
    }

    void LoadCode()
    {
        // openFile();
        const char * _tmp=getCode("file");
        std::string tmp(_tmp);
        Code = tmp;
    }
};

std::map<std::string, MyNode*(*)()> available_nodes{
    {"Compose", []() -> MyNode* { return new MyNode("Compose", {
        {"Position", NodeSlotPosition}, {"Rotation", NodeSlotRotation}  // Input slots
    }, {
        {"Matrix", NodeSlotMatrix}                                      // Output slots
    }); }},
    {"Decompose", []() -> MyNode* { return new MyNode("Decompose", {
        {"Matrix", NodeSlotMatrix}                                      // Input slots
    }, {
        {"Position", NodeSlotPosition}, {"Rotation", NodeSlotRotation}  // Output slots
    }); }},
};
std::vector<MyNode*> nodes;

namespace example
{
namespace
{
class testImNodes
{
public:
    std::string _code;

    void show()
    {
        // Canvas must be created after ImGui initializes, because constructor accesses ImGui style to configure default colors.
        static ImNodes::Ez::Context* context = ImNodes::Ez::CreateContext();
        IM_UNUSED(context);

        ImGui::SetNextWindowSize(ImVec2(512, 512), ImGuiCond_FirstUseEver);

        if (ImGui::Begin("ImNodes", nullptr, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse))
        {
            // We probably need to keep some state, like positions of nodes/slots for rendering connections.
            ImNodes::Ez::BeginCanvas();
            if (ImGui::Button("Export cmds", ImVec2(0, 0))) {
                _code = "";
                for (int i = 0; i< nodes.size(); i++)
                {
                    MyNode* node = nodes[i];
                    if (node->Connections.size()==0)
                        continue;
                    _code += node->Code.c_str();
                }
                saveMsg(_code.c_str());
            }

            for (auto it = nodes.begin(); it != nodes.end();)
            {
                MyNode* node = *it;

                // Start rendering node
                if (ImNodes::Ez::BeginNode(node, node->Title, &node->Pos, &node->Selected))
                {
                    // Render input nodes first (order is important)
                    ImNodes::Ez::InputSlots(node->InputSlots.data(), node->InputSlots.size());

                    // Custom node content may go here
                    ImGui::Text("Content of %s", node->Title);
                    if (ImGui::Button("Load Code", ImVec2(0, 0))) {
                        node->LoadCode();
                    }
                    
                    // ImGui::InputTextMultiline("code", &node->Code[0], strlen(&node->Code[0]) + 1, {222,111});
                    anhungxadieu::InputTextMultiline( "code", &node->Code, {222,111} ); // to do depth sort

                    // Render output nodes first (order is important)
                    ImNodes::Ez::OutputSlots(node->OutputSlots.data(), node->OutputSlots.size());

                    // Store new connections when they are created
                    Connection new_connection;
                    if (ImNodes::GetNewConnection(&new_connection.InputNode, &new_connection.InputSlot,
                                                &new_connection.OutputNode, &new_connection.OutputSlot))
                    {
                        ((MyNode*) new_connection.InputNode)->Connections.push_back(new_connection);
                        ((MyNode*) new_connection.OutputNode)->Connections.push_back(new_connection);
                    }

                    // Render output connections of this node
                    for (const Connection& connection : node->Connections)
                    {
                        // Node contains all it's connections (both from output and to input slots). This means that multiple
                        // nodes will have same connection. We render only output connections and ensure that each connection
                        // will be rendered once.
                        if (connection.OutputNode != node)
                            continue;

                        if (!ImNodes::Connection(connection.InputNode, connection.InputSlot, connection.OutputNode,
                                                connection.OutputSlot))
                        {
                            // Remove deleted connections
                            ((MyNode*) connection.InputNode)->DeleteConnection(connection);
                            ((MyNode*) connection.OutputNode)->DeleteConnection(connection);
                        }
                    }

                }

                // to do popup for multiline edit
                if(node->Selected && ImGui::IsWindowFocused() && ImGui::IsMouseReleased(1) && !ImGui::IsMouseDragging(1))
                {
                    js_log("go here");
                    ImGui::OpenPopup("code");
                }
                if (ImGui::BeginPopup("code"))
                {
                    ImGui::MenuItem("copy");
                    ImGui::MenuItem("paste");
                    if (ImGui::IsAnyMouseDown() && !ImGui::IsWindowHovered())
                        ImGui::CloseCurrentPopup();
                    ImGui::EndPopup();
                }

                // Node rendering is done. This call will render node background based on size of content inside node.
                ImNodes::Ez::EndNode();

                if (node->Selected && ImGui::IsKeyPressedMap(ImGuiKey_Delete) && ImGui::IsWindowFocused())
                {
                    // Deletion order is critical: first we delete connections to us
                    for (auto& connection : node->Connections)
                    {
                        if (connection.OutputNode == node)
                        {
                            ((MyNode*) connection.InputNode)->DeleteConnection(connection);
                        }
                        else
                        {
                            ((MyNode*) connection.OutputNode)->DeleteConnection(connection);
                        }
                    }
                    // Then we delete our own connections, so we don't corrupt the list
                    node->Connections.clear();
                    
                    delete node;
                    it = nodes.erase(it);
                }
                else
                    ++it;
            }

            if (ImGui::IsMouseReleased(1) && ImGui::IsWindowHovered() && !ImGui::IsMouseDragging(1))
            {
                ImGui::FocusWindow(ImGui::GetCurrentWindow());
                ImGui::OpenPopup("NodesContextMenu");
            }

            if (ImGui::BeginPopup("NodesContextMenu"))
            {
                for (const auto& desc : available_nodes)
                {
                    if (ImGui::MenuItem(desc.first.c_str()))
                    {
                        // js_log(desc.first.c_str());
                        nodes.push_back(desc.second());
                        ImNodes::AutoPositionNode(nodes.back());
                    }
                }

                ImGui::Separator();
                if (ImGui::MenuItem("Reset Zoom"))
                    ImNodes::GetCurrentCanvas()->Zoom = 1;

                if (ImGui::IsAnyMouseDown() && !ImGui::IsWindowHovered())
                    ImGui::CloseCurrentPopup();
                ImGui::EndPopup();
            }

            ImNodes::Ez::EndCanvas();
        }
        ImGui::End();
    }
};

static testImNodes editor;
} // namespace

void EXP_EditorInitialize() { }

void EXP_EditorShow() { editor.show(); }

void EXP_EditorShutdown() {}

} // namespace example
