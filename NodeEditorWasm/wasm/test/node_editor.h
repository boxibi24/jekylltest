#pragma once

namespace example
{
    void EXP_EditorInitialize();
    void EXP_EditorShow();
    void EXP_EditorShutdown();
} // namespace example