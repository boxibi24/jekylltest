#pragma once

namespace thuyShader {

static const char vertex[] = R"(#version 300 es


// ----- vertex shader start -----
in vec3 aPos;
void main()
{
   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
// ------ vertex shader end ------


)";
static const char fragment[] = R"(#version 300 es


// ----- fragment shader start ----
precision mediump float;
out vec4 FragColor;

uniform vec3 iResolution;
uniform vec4 iMouse;
uniform float iTime;

float sdSphere(vec3 p, float s)
{
    return length(p) - s;
}

void main() {
    vec2 pos = (2.0 * gl_FragCoord.xy - iResolution.xy) / iResolution.y;
    float dist = sdSphere(vec3(pos, 0.0f), .1f);
    FragColor = vec4(dist);
}
// ------ fragment shader end -----


)";

}